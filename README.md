
# HSC UDL + AutoComplete 
 
 - AutoCompletion and UDL for Halo scripts (\*.hsc)
 - Focused on MCC Reach support for now
 
 ***

## AutoComplete instructions

Move or copy `autoCompletion\HSC.xml` to the `autoCompletion` folder in your Notepad++ install location.

Example location:

- `C:\Program Files (x86)\Notepad++\autoCompletion\HSC.xml`

Restart Notepad++ if currently open.
  
#### NOTES:

Check "Enable auto-completion on each input" (Function and word completion) in `Settings` > `Preferences...` > `Auto-Completion` and change your value for "From Xth character" to whatever you want.
  
Autocomplete dropdown triggering is **Case Sensitive**. All entries from this autocomplete file begin with a **lowercase** letter.
  
Autocomplete dropdown default shortcuts:

- `Ctrl+Enter`
- `Ctrl+Space`


***

## UDL instructions

Move or copy `userDefineLangs\HSC.xml` to:

- `%AppData%\Roaming\Notepad++\userDefineLangs\HSC.xml`
   
    

Restart Notepad++ if currently open.

#### NOTES:

- UDL will be automatically applied to any .hsc file you open **after** installing this UDL.

***
